# Poser Prototype
A Godot game prototype as guided by Christine's [Let's Learn Godot 4 by Making an RPG](https://dev.to/christinec_dev/lets-learn-godot-4-by-making-an-rpg-part-1-project-overview-setup-bgc)

## Bugs
As with any learning experience, I've become familiar with Godot and the editor, and made some mistakes along the way. A few things to revisit when this is complete:
1. Autotiling tiled blank spaces where the desert sand meets any other tile
2. Godot Editor would crash when trying to create a pattern as large as a house, so the terrain is very plain
3. When autotiling, I think I set all the terrains to be in the water layer by accident. I'm not going to rewrite my map now, but I can watch out for this in the future
4. Because everything's in the water layer, none of the items autospawn ;_;
