extends Node2D

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _on_new_pressed():
	Global.change_scene("res://Scenes/Main.tscn")
	Global.scene_changed.connect(_on_scene_changed)

func _on_load_pressed():
	Global.loading = true
	Global.load_game()
	queue_free()

func _on_quit_pressed():
	get_tree().quit()

func _on_scene_changed():
	queue_free()
