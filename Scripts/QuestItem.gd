extends Area2D

# Node Refs
@onready var npc = get_tree().root.get_node("%s/SpawnedNPC/NPC" % Global.current_scene_name)

func _on_body_entered(body):
	if body.name == "Player":
		print("Quest item obtained")
		get_tree().queue_delete(self)
		npc.quest_complete = true
