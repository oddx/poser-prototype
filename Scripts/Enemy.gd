extends CharacterBody2D

# Node refs
@onready var player = get_tree().root.get_node("%s/Player" % Global.current_scene_name)
@onready var animation_sprite = $AnimatedSprite2D
@onready var animation_player = $AnimationPlayer
@onready var timer_node = $Timer
@onready var ray_cast = $RayCast2D
@onready var bullet_sfx = $GameMusic/BulletImpactMusic
@onready var shooting_sfx = $GameMusic/ShootingMusic

# Enemy stats
@export var speed = 50
var direction: Vector2
var new_direction = Vector2(0,1)
var animation
var is_attacking = false

var health = 100
var max_health = 100
var health_regen = 1

var bullet_damage = 30
var bullet_reload_time = 1000
var bullet_fired_time = 0.5

# Direction timer
var rng = RandomNumberGenerator.new()
var timer = 0

signal death

func _ready():
	rng.randomize()
	animation_sprite.modulate = Color(1,1,1)

# ---------- Damage & Health
func _process(delta):
	health = min(health + health_regen * delta, max_health)
	
	var now = Time.get_ticks_msec()
	
	if now >= bullet_fired_time:
		var target = ray_cast.get_collider()
		if target != null:
			if target.is_in_group("Player") and player.health > 0:
				shooting_sfx.play()
				is_attacking = true
				var animation = "attack_" + returned_direction(new_direction)
				animation_sprite.play(animation)
				bullet_fired_time = now + bullet_reload_time

func hit(damage):
	health -= damage
	if health > 0:
		animation_player.play("damage")
		bullet_sfx.play()
	else:
		timer_node.stop()
		direction = Vector2.ZERO
		set_process(false)
		is_attacking = true
		animation_sprite.play("death")
		player.update_xp(70)
		player.add_coins(10)
		death.emit()
		
		if rng.randf() < 0.9:
			var pickup = Global.pickups_scene.instantiate()
			pickup.item = rng.randi() % 3
			get_tree().root.get_node("%s/PickupSpawner/SpawnedPickups"  % Global.current_scene_name).call_deferred("add_child", pickup)
			pickup.position = position

func _on_animated_sprite_2d_animation_finished():
	if animation_sprite.animation == "death":
		get_tree().queue_delete(self)
	is_attacking = false
	
	if animation_sprite.animation.begins_with("attack_"):
		var bullet = Global.enemy_bullet_scene.instantiate()
		bullet.damage = bullet_damage
		bullet.direction = new_direction.normalized()
		bullet.position = player.position + new_direction.normalized() * 8
		get_tree().root.get_node("%s" % Global.current_scene_name).add_child(bullet)

# ---------- Movement & Direction ----------
func _physics_process(delta):
	var movement = speed * direction * delta
	var collision = move_and_collide(movement)
	
	if collision != null and collision.get_collider().name != "Player":
		direction = direction.rotated(randf_range(PI/4, PI/2))
		timer = randf_range(2, 5)
	else:
		timer = 0
	
	if !is_attacking:
		enemy_animations(direction)
	
	if direction != Vector2.ZERO:
		ray_cast.target_position = direction.normalized() * 50

func _on_timer_timeout():
	var player_distance = player.position - position
	if player_distance.length() <= 20:
		new_direction = player_distance.normalized()
		sync_new_direction()
	elif player_distance.length() <= 100 and timer == 0:
		direction = player_distance.normalized()
		sync_new_direction()
	elif timer <= 0:
		var random_direction = rng.randf()
		if random_direction < 0.05:
			direction = Vector2.ZERO
		elif random_direction < 0.01:
			direction = Vector2.DOWN.rotated(rng.randf() * 2 * PI)
		sync_new_direction()

# Animations

func enemy_animations(direction: Vector2):
	if direction != Vector2.ZERO:
		new_direction = direction
		animation = "walk_" + returned_direction(new_direction)
		animation_sprite.play(animation)
	else:
		animation = "idle_" + returned_direction(new_direction)
		animation_sprite.play(animation)

func returned_direction(direction: Vector2):
	var normalized_direction = direction.normalized()
	var default_return = "side"
	
	if abs(normalized_direction.x) > abs(normalized_direction.y):
		if normalized_direction.x > 0:
			# right
			$AnimatedSprite2D.flip_h = false
			return "side"
		else:
			$AnimatedSprite2D.flip_h = true
			return "side"
	elif normalized_direction.y > 0:
		return "down"
	elif normalized_direction.y < 0:
		return "up"
	
	return default_return

func sync_new_direction():
	if direction != Vector2.ZERO:
		new_direction = direction.normalized()


func _on_animation_player_current_animation_changed(name):
	animation_sprite.modulate = Color(1,1,1)

# ---------- Saving & Loading ----------
func data_to_save():
	return {
		"position": [position.x, position.y],
		"health": health,
		"max_health": max_health
	}

func data_to_load(data):
	position = Vector2(data.position[0], data.position[1])
	health = data.health
	max_health = data.max_health
