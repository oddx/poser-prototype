extends Node2D


func _on_trigger_area_body_entered(body):
	if body.is_in_group("Player"):
		Global.change_scene("res://Scenes/Main_2.tscn")

func _on_scene_changed():
	queue_free()
