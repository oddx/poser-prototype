extends Node2D

@onready var background_music = $Player/GameMusic/BackgroundMusic

func _ready():
	background_music.stream = load("res://Assets/FX/Music/Free Retro SFX by @inertsongs/Imposter Syndrome (theme).wav")
	background_music.play()

func _on_trigger_area_body_entered(body):
	if body.is_in_group("Player"):
		Global.change_scene("res://Scenes/Main.tscn")

func _on_scene_changed():
	queue_free()
