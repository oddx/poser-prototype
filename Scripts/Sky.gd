extends Node2D

@onready var animation_player = $AnimationPlayer

var current_time
var time_to_seconds
var seconds_to_timeline

func _process(delta):
	current_time = Time.get_time_dict_from_system()
	time_to_seconds = current_time.hour * 3600 + current_time.minute * 60 + current_time.second
	seconds_to_timeline = remap(time_to_seconds, 0, 86400, 0, 24)
	animation_player.seek(seconds_to_timeline)
	animation_player.play("day_night_cycle")
