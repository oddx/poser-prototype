### Player.gd
extends CharacterBody2D
# Custom signals
signal health_updated
signal stamina_updated
signal ammo_pickups_updated
signal health_pickups_updated
signal stamina_pickups_updated
signal xp_updated
signal level_updated
signal xp_requirements_updated
signal coins_updated

# Pickups
var ammo_pickup = 10
var stamina_pickup = 0
var health_pickup = 0
var coins = 200

# Node references
@onready var animation_sprite = $AnimatedSprite2D
@onready var health_bar = $UI/HealthBar
@onready var stamina_bar = $UI/StaminaBar
@onready var ammo_amount = $UI/AmmoAmount
@onready var stamina_amount = $UI/StaminaAmount
@onready var health_amount = $UI/HealthAmount
@onready var xp_amount = $UI/XP
@onready var level_amount = $UI/Level
@onready var animation_player = $AnimationPlayer
@onready var level_popup = $UI/LevelPopup
@onready var ray_cast = $RayCast2D
@onready var pause_screen = $UI/PauseScreen
@onready var coin_amount = $UI/CoinAmount
@onready var pause_menu_music = $GameMusic/PauseMenuMusic
@onready var background_music = $GameMusic/BackgroundMusic
@onready var game_over_music = $GameMusic/GameOverMusic
@onready var dialog_music = $GameMusic/DialogMusic
@onready var level_up_music = $GameMusic/LevelUpMusic
@onready var pickups_sfx = $GameMusic/PickupsMusic
@onready var consume_sfx = $GameMusic/ConsumableMusic
@onready var bullet_sfx = $GameMusic/BulletImpactMusic
@onready var shooting_sfx = $GameMusic/ShootingMusic

# Player movement speed
@export var speed = 50

var is_attacking = false

# UI variables
var health = 100
var max_health = 100
var regen_health = 1
var stamina = 100
var max_stamina = 100
var regen_stamina = 5

var bullet_damage = 30
var bullet_reload_time = 1000
var bullet_fired_time = 0.5

var new_direction = Vector2(0,1)
var animation

var xp = 0
var level = 1
var xp_requirements = 100

var paused

# ---------- Movement & Animation ----------
func _physics_process(delta):
	var direction: Vector2
	direction.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	direction.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	
	if abs(direction.x) == 1 and abs(direction.y) == 1:
		direction = direction.normalized()
	
	if Input.is_action_pressed("ui_sprint"):
		if stamina >= 25:
			speed = 100
			stamina = stamina - 1
			stamina_updated.emit(stamina, max_stamina)
	elif Input.is_action_just_released("ui_sprint"):
		speed = 50
	
	var movement = speed * direction * delta
	
	if is_attacking == false:
		move_and_collide(movement)
		player_animations(direction)
	
	if !Input.is_anything_pressed():
		if is_attacking == false:
			animation = "idle_" + returned_direction(new_direction)
	
	if direction != Vector2.ZERO:
		ray_cast.target_position = direction.normalized() * 50

func _input(event):
	if event.is_action_pressed("ui_attack"):
		var now = Time.get_ticks_msec()
		if now >= bullet_fired_time and ammo_pickup > 0:
			shooting_sfx.play()
			is_attacking = true
			var animation = "attack_" + returned_direction(new_direction)
			animation_sprite.play(animation)
			bullet_fired_time = now + bullet_reload_time
			ammo_pickup = ammo_pickup - 1
			ammo_pickups_updated.emit(ammo_pickup)
	elif event.is_action_pressed("ui_consume_health"):
		if health > 0 && health_pickup > 0:
			health_pickup = health_pickup - 1
			health = min(health + 50, max_health)
			health_updated.emit(health, max_health)
			health_pickups_updated.emit(health_pickup)
			consume_sfx.play()
	elif event.is_action_pressed("ui_consume_stamina"):
		if stamina > 0 && stamina_pickup > 0:
			stamina_pickup = stamina_pickup - 1
			stamina = min(stamina + 50, max_stamina)
			stamina_updated.emit(stamina, max_stamina)
			stamina_pickups_updated.emit(stamina_pickup)
			consume_sfx.stream = load("res://Assets/FX/Music/Free Retro SFX by @inertsongs/SFX/stam0.wav")
			consume_sfx.play()
	elif event.is_action_pressed("ui_interact"):
		var target = ray_cast.get_collider()
		if target != null:
			if target.is_in_group("NPC"):
				target.dialog()
				background_music.stop()
				dialog_music.play()
				return
			if target.name == "Bed":
				animation_player.play("sleeping")
				health = max_health
				stamina = max_stamina
				health_updated.emit(health, max_health)
				stamina_updated.emit(stamina, max_stamina)
				return
	elif !pause_screen.visible:
		if event.is_action_pressed("ui_pause"):
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			get_tree().paused = true
			pause_screen.visible = true
			set_physics_process(false)
			paused = true
			background_music.stop()
			pause_menu_music.play()
			
			if health <= 0:
				get_node("/root/%s" % Global.current_scene_name).queue_free()
				Global.change_scene("res://Scenes/MainScene.tscn")
				get_tree().paused = false
				return

# ---------- Damage & Health ---------- #
func hit(damage):
	health -= damage
	health_updated.emit(health, max_health)
	if health > 0:
		animation_player.play("damage")
		health_updated.emit(health, max_health)
		bullet_sfx.play()
	else:
		set_process(false)
		get_tree().paused = true
		paused = true
		animation_player.play("game_over")
		background_music.stop()
		game_over_music.play()

# Animations

func player_animations(direction: Vector2):
	if direction != Vector2.ZERO:
		new_direction = direction
		animation = "walk_" + returned_direction(new_direction)
		animation_sprite.play(animation)
	else:
		animation = "idle_" + returned_direction(new_direction)
		animation_sprite.play(animation)

func returned_direction(direction: Vector2):
	var normalized_direction = direction.normalized()
	var default_return = "side"
	
	if normalized_direction.y > 0:
		return "down"
	elif normalized_direction.y < 0:
		return "up"
	elif normalized_direction.x > 0:
		# right
		$AnimatedSprite2D.flip_h = false
		return "side"
	elif normalized_direction.x < 0:
		$AnimatedSprite2D.flip_h = true
		return "side"
	
	return default_return


func _on_animated_sprite_2d_animation_finished():
	is_attacking = false
	if animation_sprite.animation.begins_with("attack_"):
		var bullet = Global.bullet_scene.instantiate()
		bullet.damage = bullet_damage
		bullet.direction = new_direction.normalized()
		bullet.position = position + new_direction.normalized() * 4
		get_tree().root.get_node("%s" % Global.current_scene_name).add_child(bullet)

func _ready():
	health_updated.connect(health_bar.update_health_ui)
	stamina_updated.connect(stamina_bar.update_stamina_ui)
	ammo_pickups_updated.connect(ammo_amount.update_ammo_pickup_ui)
	health_pickups_updated.connect(health_amount.update_health_pickup_ui)
	stamina_pickups_updated.connect(stamina_amount.update_stamina_pickup_ui)
	xp_updated.connect(xp_amount.update_xp_ui)
	xp_requirements_updated.connect(xp_amount.update_xp_requirements_ui)
	level_updated.connect(level_amount.update_level_ui)
	coins_updated.connect(coin_amount.update_coin_amount_ui)
	
	animation_sprite.modulate = Color(1,1,1,1)
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	
	$UI/AmmoAmount/Value.text = str(ammo_pickup)
	$UI/StaminaAmount/Value.text = str(stamina_pickup)
	$UI/HealthAmount/Value.text = str(health_pickup)
	$UI/XP/Value.text = str(xp)
	$UI/XP/Value2.text = "/ " + str(xp_requirements)
	$UI/Level/Value.text = str(level)

# ---------- UI ----------
func _process(delta):
	# calculate health
	var updated_health = min(health + regen_health * delta, max_health)
	if updated_health != health:
		health = updated_health
		health_updated.emit(health, max_health)

	# calculate stamina
	var updated_stamina = min(stamina + regen_stamina * delta, max_stamina)
	if updated_stamina != stamina:
		stamina = updated_stamina
		stamina_updated.emit(stamina, max_stamina)

# ---------- Consumables ----------
func add_pickup(item):
	if item == Global.Pickups.AMMO:
		ammo_pickup = ammo_pickup + 3
		ammo_pickups_updated.emit(ammo_pickup)
	if item == Global.Pickups.HEALTH:
		health_pickup = health_pickup + 1
		health_pickups_updated.emit(health_pickup)
	if item == Global.Pickups.STAMINA:
		stamina_pickup = stamina_pickup + 1
		stamina_pickups_updated.emit(stamina_pickup)
	pickups_sfx.play()
	update_xp(5)


func _on_animation_player_animation_finished(anim_name):
	animation_sprite.modulate = Color(1,1,1,1)

func add_coins(coins_amount):
	coins += coins_amount
	coins_updated.emit(coins)

# ---------- Level & XP ----------
func update_xp(value):
	xp += value
	
	if xp >= xp_requirements:
		background_music.stop()
		level_up_music.play()
		set_process_input(true)
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		get_tree().paused = true
		level_popup.visible = true
		
		xp = 0
		level += 1
		xp_requirements *= 2
		
		max_health += 10
		max_stamina += 10
		
		ammo_pickup += 10
		health_pickup += 5
		stamina_pickup += 3
		
		health_updated.emit(health, max_health)
		stamina_updated.emit(stamina, max_stamina)
		ammo_pickups_updated.emit(ammo_pickup)
		health_pickups_updated.emit(health_pickup)
		stamina_pickups_updated.emit(stamina_pickup)
		xp_updated.emit(xp)
		level_updated.emit(level)
		
		$UI/LevelPopup/Message/Rewards/LevelGained.text = "LVL: " + str(level)
		$UI/LevelPopup/Message/Rewards/HealthincreaseGained.text = "MAX HP: " + str(max_health)
		$UI/LevelPopup/Message/Rewards/StaminaIncreaseGained.text = "MAX SP: " + str(max_stamina)
		$UI/LevelPopup/Message/Rewards/HealthPickupsGained.text = "+ HEALTH: 5"
		$UI/LevelPopup/Message/Rewards/StaminaPickupsGained.text = "+ STAMINA: 3"
		$UI/LevelPopup/Message/Rewards/AmmoPickupsGained.text = "+ AMMO: 10"
	
	xp_requirements_updated.emit(xp_requirements)
	xp_updated.emit(xp)
	level_updated.emit(level)



func _on_confirm_pressed():
	level_popup.visible = false
	get_tree().paused = false
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

# ---------- Pause Screen ---------

func _on_resume_pressed():
	pause_screen.visible = false
	get_tree().paused = false
	paused = false
	set_process_input(true)
	set_physics_process(true)
	pause_menu_music.stop()
	background_music.play()


func _on_save_pressed():
	Global.save()


func _on_quit_pressed():
	Global.change_scene("res://Scenes/MainScene.tscn")
	get_tree().paused = false
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

# ---------- Saving & Loading ----------
func data_to_save():
	return {
		"position": [position.x, position.y],
		"health": health,
		"max_health": max_health,
		"stamina": stamina,
		"max_stamina": max_stamina,
		"xp": xp,
		"xp_requirements": xp_requirements,
		"level": level,
		"ammo_pickup": ammo_pickup,
		"health_pickup": health_pickup,
		"stamina_pickup": stamina_pickup,
		"coins": coins
	}

func data_to_load(data):
	position = Vector2(data.position[0], data.position[1])
	health = data.health
	max_health = data.max_health
	stamina = data.stamina
	max_stamina = data.max_stamina
	xp = data.xp
	xp_requirements = data.xp_requirements
	level = data.level
	ammo_pickup = data.ammo_pickup
	health_pickup = data.health_pickup
	stamina_pickup = data.stamina_pickup
	coins = data.coins

func values_to_load(data):
	health = data.health
	max_health = data.max_health
	stamina = data.stamina
	max_stamina = data.max_stamina
	xp = data.xp
	xp_requirements = data.xp_requirements
	level = data.level
	ammo_pickup = data.ammo_pickup
	health_pickup = data.health_pickup
	stamina_pickup = data.stamina_pickup
	coins = data.coins
	
	health_updated.emit(health, max_health)
	stamina_updated.emit(stamina, max_stamina)
	ammo_pickups_updated.emit(ammo_pickup)
	health_pickups_updated.emit(health_pickup)
	stamina_pickups_updated.emit(stamina_pickup)
	xp_updated.emit(xp)
	level_updated.emit(level)
	coins_updated.emit(coins)
	
	$UI/AmmoAmount/Value.text = str(data.ammo_pickup)
	$UI/StaminaAmount/Value.text = str(data.stamina_pickup)
	$UI/HealthAmount/Value.text = str(data.health_pickup)
	$UI/XP/Value.text = str(data.xp)
	$UI/XP/Value2.text = "/ " + str(data.xp_requirements)
	$UI/Level/Value.text = str(data.level)
	$UI/CoinAmount/Value.text = str(data.coins)
