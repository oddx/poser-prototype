extends Node

@onready var pickups_scene = preload("res://Scenes/Pickup.tscn")
@onready var enemy_scene = preload("res://Scenes/Enemy.tscn")
@onready var bullet_scene = preload("res://Scenes/Bullet.tscn")
@onready var enemy_bullet_scene = preload("res://Scenes/EnemyBullet.tscn")

signal scene_changed()

# Pickups
enum Pickups { AMMO, STAMINA, HEALTH }

# TileMap layers
const WATER_LAYER = 0
const GRASS_LAYER = 1
const SAND_LAYER = 2
const FOLIAGE_LAYER = 3

var current_scene_name
var save_path = "user://dusty_trails_save.json"
var loading = false

func _ready():
	current_scene_name = get_tree().get_current_scene().name

func change_scene(scene_path):
	current_scene_name = scene_path.get_file().get_basename()
	var current_scene = get_tree().get_root().get_child(get_tree().get_root().get_child_count() - 1)
	current_scene.queue_free()
	var new_scene = load(scene_path).instantiate()
	get_tree().get_root().call_deferred("add_child", new_scene)
	get_tree().call_deferred("set_current_scene", new_scene)
	call_deferred("post_scene_change_initialization")

func post_scene_change_initialization():
	load_data()
	scene_changed.emit()

# ----------- Saving & Loading ----------
func save():
	var current_scene = get_tree().get_current_scene()
	if current_scene != null:
		current_scene_name = current_scene.name
		var data = {
			"scene_name": current_scene_name
		}
		if current_scene.has_node("Player"):
			var player = get_tree().get_root().get_node("%s/Player" % current_scene_name)
			print("Player exists: ", player != null)
			data["player"] = player.data_to_save()
		if current_scene.has_node("SpawnedNPC/NPC"):
			var npc = get_tree().get_root().get_node("%s/SpawnedNPC/NPC" % current_scene_name)
			print("NPC esists: ", npc != null)
			data["npc"] = npc.data_to_save()
		if current_scene.has_node("EnemySpawner"):
			var enemy_spawner = get_tree().get_root().get_node("%s/EnemySpawner" % current_scene_name)
			print("EnemySpawner esists: ", enemy_spawner != null)
			data["enemies"] = enemy_spawner.data_to_save()
		var json = JSON.new()
		var to_json = json.stringify(data)
		var file = FileAccess.open(save_path, FileAccess.WRITE)
		file.store_line(to_json)
		file.close()
	else:
		print("No active scene. Cannot save.")

func load_game():
	if loading and FileAccess.file_exists(save_path):
		print("Save file found!")
		var file = FileAccess.open(save_path, FileAccess.READ)
		var data = JSON.parse_string(file.get_as_text())
		file.close()
		var scene_path = "res://Scenes/%s.tscn" % data["scene_name"]
		var game_resource = load(scene_path)
		var game = game_resource.instantiate()
		get_tree().root.call_deferred("add_child", game)
		get_tree().call_deferred("set_current_scene", game)
		current_scene_name = game.name
		var player = game.get_node("Player")
		var npc = game.get_node("SpawnedNPC/NPC")
		var enemy_spawner = game.get_node("EnemySpawner")
		if player:
			player.data_to_load(data["player"])
		if npc:
			npc.data_to_load(data["npc"])
		if enemy_spawner:
			enemy_spawner.data_to_load(data["enemies"])
		if(npc and npc.quest_complete):
			game.get_node("SpawnedQuestItems/QuestItem").queue_free()
	else:
		print("Save file not found!")

func load_data():
	var current_scene = get_tree().get_current_scene()
	if current_scene and FileAccess.file_exists(save_path):
		print("Save file found!")
		var file = FileAccess.open(save_path, FileAccess.READ)
		var data = JSON.parse_string(file.get_as_text())
		file.close()
		var player = current_scene.get_node("Player")
		if player and data.has("player"):
			player.values_to_load(data["player"])
	else:
		print("Save file not found!")
