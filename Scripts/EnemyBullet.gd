extends Area2D

@onready var tilemap = get_tree().root.get_node("%s/Map" % Global.current_scene_name)
@onready var animated_sprite = $AnimatedSprite2D
var speed = 80
var direction : Vector2
var damage

func _process(delta):
	position = position + speed * delta  * direction

func _on_body_entered(body):
	if body.name == "Enemy":
		return
	
	# ignore water collisions
	if body.name == "Map" and tilemap.get_layer_name(Global.WATER_LAYER):
		return
	
	if body.is_in_group("Player"):
		body.hit(damage)
	
	direction = Vector2.ZERO
	animated_sprite.play("impact")


func _on_animated_sprite_2d_animation_finished():
	if animated_sprite.animation == "impact":
		get_tree().queue_delete(self)


func _on_timer_timeout():
	animated_sprite.play("impact")
