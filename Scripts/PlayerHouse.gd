extends Node2D

# Node Refs
@onready var exterior = $Exterior
@onready var interior = $Interior

func _on_trigger_area_body_entered(body):
	if body.is_in_group("Player"):
		interior.show()
		exterior.hide()
	elif body.is_in_group("enemy"):
		body.direction = -body.direction
		body.timer = 16


func _on_trigger_area_body_exited(body):
	if body.is_in_group("Player"):
		interior.hide()
		exterior.show()
