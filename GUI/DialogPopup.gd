extends CanvasLayer

# Node refs
@onready var animation_player = $"../../AnimationPlayer"
@onready var player = $"../.."
@onready var background_music = $"../../GameMusic/BackgroundMusic"
@onready var dialog_music = $"../../GameMusic/DialogMusic"

# the set property runs a specific function on access
var npc_name: set = npc_name_set
var message: set = message_set
var response: set = response_set

# npc reference
var npc

func npc_name_set(new_value):
	npc_name = new_value
	$Dialog/NPC.text = new_value

func message_set(new_value):
	message = new_value
	$Dialog/Message.text = new_value

func response_set(new_value):
	response = new_value
	$Dialog/Response.text = new_value

# ---------- Processing ----------
func _ready():
	set_process_input(false)

func open():
	get_tree().paused = true
	self.visible = true
	animation_player.play("typewriter")
	player.set_physics_process(false)
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func close():
	get_tree().paused = false
	self.visible = false
	player.set_physics_process(true)
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	dialog_music.stop()
	background_music.play()

func _on_animation_player_animation_finished(anim_name):
	set_process_input(true)

# ----------- Dialog -----------
func _input(event):
	if event is InputEventKey:
		if event.keycode == KEY_A:
			npc.dialog("A")
		elif event.keycode == KEY_B:
			npc.dialog("B")
