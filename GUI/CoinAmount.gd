extends ColorRect

@onready var value = $Value
@onready var player = $"../.."

func _ready():
	value.text = str(player.coins)

func update_coin_amount_ui(coin_amount):
	value.text = str(coin_amount)
