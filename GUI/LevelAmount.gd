extends ColorRect

# Node refs
@onready var value = $Value
@onready var player = $"../.."

func _ready():
	value.text = str(player.level)

func update_level_ui(level):
	value.text = str(level)
