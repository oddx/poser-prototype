extends ColorRect

# Node refs
@onready var value = $Value
@onready var value2 = $Value2
@onready var player = $"../.."

func _ready():
	value.text = str(player.xp)
	value2.text = "/" + str(player.xp_requirements)

func update_xp_ui(xp):
	value.text = str(xp)

func update_xp_requirements_ui(xp_requirements):
	value2.text =  "/" + str(xp_requirements)
